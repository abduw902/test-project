import 'package:flutter/material.dart';

const Color kBlack = Color.fromRGBO(51, 51, 51, 1);
const Color kGrey = Color.fromRGBO(130, 130, 130, 1);
const Color kLightGrey = Color.fromRGBO(237, 237, 237, 1);
const Color kBlue = Color.fromRGBO(45, 156, 219, 1);
const Color kWhite = Color.fromRGBO(255, 255, 255, 1);

const kSemiBoldBlack16 =
    TextStyle(fontWeight: FontWeight.w600, fontSize: 16.0, color: kBlack);

const kMediumBlack16 =
    TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0, color: kBlack);
const kMediumBlack14 =
    TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0, color: kBlack);

const kRegularBlack16 =
    TextStyle(fontWeight: FontWeight.w400, fontSize: 16.0, color: kBlack);
const kRegularBlack14 =
    TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kBlack);

const kRegularGrey14 =
    TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kGrey);

const kRegularBlue14 =
    TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: kBlue);

