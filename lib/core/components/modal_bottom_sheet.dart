import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';

Future<void> customModalBottomSheet(
        BuildContext context, String title, Widget child) =>
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      backgroundColor: kBlue,
      builder: (BuildContext context) {
        return AnimatedPadding(
          padding: MediaQuery.of(context).viewInsets,
          duration: const Duration(milliseconds: 100),
          curve: Curves.decelerate,
          child: IntrinsicHeight(
            child: Column(
              children: [
                verticalSpace16,
                Row(
                  children: [
                    const Spacer(),
                    horizontalSpace25,
                    Text(
                      title,
                      style: kSemiBoldBlack16.copyWith(color: kWhite),
                    ),
                    const Spacer(),
                    GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: const Icon(Icons.close,color: kWhite,),
                        ),
                    horizontalSpace20
                  ],
                ),
                SizedBox(
                  child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Container(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        color: Colors.white,
                        child: child,
                      )),
                ),
              ],
            ),
          ),
        );
      },
    );
