import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';

class BuildLoading extends StatelessWidget {
  const BuildLoading();

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(
        color: kBlue,
      ),
    );
  }
}
