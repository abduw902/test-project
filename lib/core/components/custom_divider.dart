import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';

class CustomDivider extends StatelessWidget {
  final double? height;

  const CustomDivider({this.height});

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: height ?? 1.0,
      thickness: 1.0,
      color: kLightGrey,
    );
  }
}
