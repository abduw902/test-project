import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final AppBar appBar = AppBar();
  final bool isBackgroundColorTransparent;

  CustomAppBar({
    Key? key,
    this.title,
    this.isBackgroundColorTransparent = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor:
          isBackgroundColorTransparent ? Colors.transparent : kWhite,
      centerTitle: true,
      title: Text(title ?? '', style: kSemiBoldBlack16),
      iconTheme: IconThemeData(
        color: isBackgroundColorTransparent ? kWhite : kBlack,
      ),
      elevation: 0,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
