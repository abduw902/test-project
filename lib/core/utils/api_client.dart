import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/platform_access/network_state.dart';

import 'http_utils.dart';

abstract class ApiClient {
  ///Get Request
  static Future<dynamic> getRequest(
    String url,
  ) async =>
      _request(HttpUtil.request(url, requestType: get));

  ///Post Request
  static Future<dynamic> postRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: post, body: body));

  ///Put Request
  static Future<dynamic> putRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: put, body: body));

  ///Patch Request
  static Future<dynamic> patchRequest(
          String url, Map<String, dynamic> body) async =>
      _request(HttpUtil.request(url, requestType: patch, body: body));

  ///Delete Request
  static Future<dynamic> deleteRequest(String url,
          {Map<String, dynamic>? body}) async =>
      _request(HttpUtil.request(url, requestType: delete, body: body ?? {}));

  ///MultiPart Request
  static Future<dynamic> multiPartUpload(
          String uploadingDataType, String url, String filePath) async =>
      _request(HttpUtil.uploadMultiPart(uploadingDataType, url, filePath),
          isMultiPartRequest: true);

  static Future<dynamic> _request(Future<Response> request,
      {bool isMultiPartRequest = false}) async {
    if (!await NetworkState.isInternetConnected()) {
      throw const SocketException("Нет интернет соединения");
    }
    final Response response = await request;

    if (response.statusCode == 200 || response.statusCode == 201) {
      if (isMultiPartRequest) {
        return json.decode(utf8.decode(response.bodyBytes));
      }

      if (response.body.isNotEmpty) {
        return json.decode(utf8.decode(response.bodyBytes));
      }

      return;
    } else {
      throw ServerException(int.parse(response.statusCode.toString()));
    }
  }
}
