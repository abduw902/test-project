import 'dart:convert';

import 'package:http/http.dart';
import 'package:test_project/core/error/exception.dart';

// ignore: avoid_classes_with_only_static_members
abstract class HttpUtil {
  static Future<Response> request(String url,
      {Map<String, String>? headers,
      dynamic body,
      required Function requestType}) async {
    try {
      print(url);

      final httpHeaders = <String, String>{'Content-Type': 'application/json'};

      if (headers != null) httpHeaders.addAll(headers);

      dynamic response;

      if (requestType == get) {
        response = await requestType(Uri.parse(url), headers: httpHeaders);
      } else {
        print("Request body: ${body ?? ''}");

        response = await requestType(Uri.parse(url),
            headers: httpHeaders, body: jsonEncode(body));
      }

      print("Response body:  ${response.body}");

      return response as Response;
    } catch (e) {
      print(
          "Error caught in request() method inside http_utils.dart file:\n $e");
      throw const ServerException(-1);
    }
  }

  static Future<Response> uploadMultiPart(
    ///'image', 'file', 'video;
    String uploadingDataType,
    String url,
    String filePath,
  ) async {
    print(url);

    try {
      final request = MultipartRequest('POST', Uri.parse(url))
        ..files.add(await MultipartFile.fromPath(
          uploadingDataType,
          filePath,
        ));

      final response = await Response.fromStream(await request.send());

      print("Response StatusCode: ${response.statusCode}");
      print("Response body: ${response.body}");

      return response;
    } catch (e) {
      print(
          "Error caught in uploadMultiPart() method inside http_utils.dart file\n $e");

      throw const ServerException(-1);
    }
  }
}
