class HiveKeys {
  static const String userId = 'userId';
  static const String name = 'name';
  static const String surname = 'surname';
  static const String gender = 'gender';
  static const String userImgUrl = 'userImgUrl';
  static const String authorization = 'authorization';
  static const String isFirstVisit = 'isFirstVisit';
  static const String deviseFingerPrint = 'deviseFingerPrint';
}
