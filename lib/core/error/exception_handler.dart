import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';

Future<Either<String, T>> exceptionHandler<T>(
  Future<Either<String, T>> Function() tryThis,
) async {
  try {
    return await tryThis();
  } on ServerException {
    return const Left(serverErrorMsg);
  } on CacheException {
    return const Left(cacheErrorMsg);
  } on SocketException {
    return const Left(noInternetErrorMsg);
  }
}
