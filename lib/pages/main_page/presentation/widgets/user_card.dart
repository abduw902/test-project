import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';

class UserCard extends StatelessWidget {
  final String user;
  final String userName;

  const UserCard({
    Key? key,
    required this.user,
    required this.userName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration:
          BoxDecoration(color: kWhite, borderRadius: BorderRadius.circular(20)),
      child: Column(
        children: [
          Text(
            userName,
            style: kSemiBoldBlack16,
            overflow: TextOverflow.ellipsis,
          ),
          verticalSpace16,
          Text(
            user,
            style: kRegularGrey14,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }
}
