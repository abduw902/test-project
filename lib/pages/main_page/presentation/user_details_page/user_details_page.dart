import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/data/model/users_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/albums_preview.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/posts_preview.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/text_with_preffix.dart';

class UserDetailsPage extends StatefulWidget {
  final UserModel userModel;

  const UserDetailsPage({Key? key, required this.userModel}) : super(key: key);

  @override
  _UserDetailsPageState createState() => _UserDetailsPageState();
}

class _UserDetailsPageState extends State<UserDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      appBar: CustomAppBar(
        title: widget.userModel.username,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Personal information:',
              style: kSemiBoldBlack16,
            ),
            TextWithPrefix(prefix: 'name:', text: widget.userModel.name!),
            TextWithPrefix(
              prefix: 'email:',
              text: widget.userModel.email!,
              textColor: kBlue,
            ),
            TextWithPrefix(prefix: 'phone:', text: widget.userModel.phone!),
            TextWithPrefix(prefix: 'website:', text: widget.userModel.website!),
            const CustomDivider(),
            verticalSpace8,
            const Text(
              'Working company:',
              style: kSemiBoldBlack16,
            ),
            TextWithPrefix(
                prefix: 'company name:', text: widget.userModel.company!.name!),
            TextWithPrefix(prefix: 'bs:', text: widget.userModel.company!.bs!),
            TextWithPrefix(
                prefix: 'catch phrase:',
                text: widget.userModel.company!.catchPhrase!,
                textFontStyle: FontStyle.italic),
            const CustomDivider(),
            verticalSpace8,
            const Text(
              'Address:',
              style: kSemiBoldBlack16,
            ),
            TextWithPrefix(
                prefix: 'street:', text: widget.userModel.address!.street!),
            TextWithPrefix(
                prefix: 'suite:', text: widget.userModel.address!.suite!),
            TextWithPrefix(
                prefix: 'city:', text: widget.userModel.address!.city!),
            TextWithPrefix(
                prefix: 'zipcode:', text: widget.userModel.address!.zipcode!),
            PostsPreview(userId: widget.userModel.id!),
            AlbumsPreview(userId: widget.userModel.id!),
          ],
        ),
      ),
    );
  }
}
