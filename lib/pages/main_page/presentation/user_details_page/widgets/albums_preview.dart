import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/loading_widget.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/core/utils/toast_utils.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/cubit/albums_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/model/albums_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/repository/albums_repository.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/presentation/all_albums_of_user.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/cubit/posts_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/photos_list.dart';

class AlbumsPreview extends StatefulWidget {
  final int userId;

  const AlbumsPreview({Key? key, required this.userId}) : super(key: key);

  @override
  _AlbumsPreviewState createState() => _AlbumsPreviewState();
}

class _AlbumsPreviewState extends State<AlbumsPreview> {
  late AlbumsCubit _cubit;
  List<AlbumModel> albumModels = [];

  @override
  void initState() {
    super.initState();
    _cubit = AlbumsCubit(AlbumsRepositoryImplements())
      ..getAlbumsOfUser(userId: widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AlbumsCubit, AlbumsState>(
      bloc: _cubit,
      listener: (context, state) {
        if (state is AlbumsLoadingErrorState) {
          ToastUtils.showShortToast(context, state.errorMessage);
        }
        if (state is AlbumsLoadedState) {
          albumModels = state.albumModels;
        }
      },
      builder: (context, state) {
        if (state is AlbumsLoadingErrorState) {
          return Container();
        }
        if (state is AlbumsLoadingState) {
          return const BuildLoading();
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            verticalSpace8,
            const CustomDivider(),
            verticalSpace8,
            const Text(
              'Albums:',
              style: kSemiBoldBlack16,
            ),
            verticalSpace8,
            ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        albumModels[index].title!,
                        style: kMediumBlack14,
                      ),
                      verticalSpace5,
                      PhotosList(albumId: albumModels[index].id!)
                    ],
                  );
                },
                separatorBuilder: (context, index) => verticalSpace8,
                itemCount: 3),
            verticalSpace8,
            GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AllAlbumsOfUser(
                            albumModels: albumModels,
                          ))),
              child: const Center(
                child: Text(
                  'All albums',
                  style: kRegularBlue14,
                ),
              ),
            )
          ],
        );
        ;
      },
    );
  }
}
