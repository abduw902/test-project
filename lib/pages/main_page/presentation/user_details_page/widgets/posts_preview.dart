import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/loading_widget.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/core/utils/toast_utils.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/cubit/posts_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/repository/posts_repository.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/all_posts_of_user.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/post_details.dart';

class PostsPreview extends StatefulWidget {
  final int userId;

  const PostsPreview({Key? key, required this.userId}) : super(key: key);

  @override
  _PostsPreviewState createState() => _PostsPreviewState();
}

class _PostsPreviewState extends State<PostsPreview> {
  late PostsCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = PostsCubit(PostsRepositoryImplements())
      ..getPostsOfUser(userId: widget.userId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PostsCubit, PostsState>(
      bloc: _cubit,
      listener: (context, state) {
        if (state is PostsLoadingErrorState) {
          ToastUtils.showShortToast(context, state.errorMessage);
        }
      },
      builder: (context, state) {
        if (state is PostsLoadingErrorState) {
          return Container();
        }
        if (state is PostsLoadedState) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomDivider(),
              verticalSpace8,
              const Text(
                'Posts:',
                style: kSemiBoldBlack16,
              ),
              verticalSpace8,
              ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PostDetails(
                                  postModel: state.postModels[index]))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.postModels[index].title!,
                            style: kMediumBlack14,
                          ),
                          verticalSpace5,
                          Text(
                            state.postModels[index].body!,
                            style: kRegularGrey14,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          )
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => verticalSpace8,
                  itemCount: 3),
              verticalSpace8,
              GestureDetector(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AllPostsOfUser(
                              postModels: state.postModels,
                            ))),
                child: const Center(
                  child: Text(
                    'All posts',
                    style: kRegularBlue14,
                  ),
                ),
              )
            ],
          );
        }
        return const BuildLoading();
      },
    );
  }
}
