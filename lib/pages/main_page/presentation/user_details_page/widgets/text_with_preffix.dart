import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';

class TextWithPrefix extends StatelessWidget {
  final String? prefix;
  final String text;
  final Color? textColor;
  final Color? prefixColor;
  final FontStyle? textFontStyle;

  const TextWithPrefix(
      {Key? key,
      this.prefix,
      required this.text,
      this.textColor,
      this.prefixColor, this.textFontStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 100,
            child: Text(
              prefix ?? '',
              style: kRegularGrey14.copyWith(color: prefixColor),
            ),
          ),
          Flexible(
            child: Text(
              text,
              style: kRegularBlack16.copyWith(color: textColor, fontStyle: textFontStyle),
            ),
          ),
        ],
      ),
    );
  }
}
