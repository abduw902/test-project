import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/presentation/album_details_page/album_details.dart';
import 'package:test_project/core/components/loading_widget.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/cubit/photos_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/data/repository/albums_repository.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/photos.dart';

class PhotosList extends StatefulWidget {
  final int albumId;

  const PhotosList({Key? key, required this.albumId}) : super(key: key);

  @override
  _PhotosListState createState() => _PhotosListState();
}

class _PhotosListState extends State<PhotosList> {
  late PhotosCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = PhotosCubit(PhotosRepositoryImplements());
    _cubit.getAlbumPhotos(albumId: widget.albumId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhotosCubit, PhotosState>(
      bloc: _cubit,
      builder: (context, state) {
        if (state is AlbumPhotosLoadedState) {
          return SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                for (int index = 0;
                    index < state.photoModels.length;
                    index++) ...[
                  Photos(
                    imageUrl: state.photoModels[index].thumbnailUrl!,
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => AlbumDetails(
                                backgroundColor: kBlack,
                                photoModels: state.photoModels,
                                startsWith: index,
                              )));
                    },
                  ),
                  horizontalSpace8,
                ]
              ],
            ),
          );
        }
        return const BuildLoading();
      },
    );
  }
}
