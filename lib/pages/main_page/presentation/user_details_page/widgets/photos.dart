import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:test_project/core/components/shimmer_widget.dart';
import 'package:test_project/core/utils/constants/api.dart';

class Photos extends StatelessWidget {
  final String? imageUrl;
  final VoidCallback onTap;

  const Photos({this.imageUrl, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: CachedNetworkImage(
            height: 50.0,
            width: 50.0,
            imageUrl: '$imageUrl',
            fit: BoxFit.cover,
            placeholder: (context, _) => ShimmerWidget.rectangular(
                height: 50.0,
                shapeBorder: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0))),
            errorWidget: (ctx, _, __) {
              return ShimmerWidget.rectangular(
                  height: 50.0,
                  shapeBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0)));
            },
          )),
    );
  }
}
