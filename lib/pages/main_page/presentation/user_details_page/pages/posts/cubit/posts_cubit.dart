import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/model/posts_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/repository/posts_repository.dart';

part 'posts_state.dart';

class PostsCubit extends Cubit<PostsState> {
  final PostsRepository repository;

  PostsCubit(this.repository) : super(PostsInitial());

  // Future<void> getAllPosts() async {
  //   try {
  //     emit(PostsLoadingState());
  //     final result = await repository.getAllPosts();
  //     emit(PostsLoadedState(result));
  //   } on ServerException {
  //     emit(PostsLoadingErrorState(serverErrorMsg));
  //   } on SocketException {
  //     emit(PostsLoadingErrorState(noInternetErrorMsg));
  //   } catch (e) {
  //     emit(PostsLoadingErrorState(e.toString()));
  //   }
  // }

  Future<void> getPostsOfUser({required int userId}) async {
    try {
      emit(PostsLoadingState());
      final result = await repository.getPostsOfUser(userId);
      emit(PostsLoadedState(result));
    } on ServerException {
      emit(PostsLoadingErrorState(serverErrorMsg));
    } on SocketException {
      emit(PostsLoadingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(PostsLoadingErrorState(e.toString()));
    }
  }
}
