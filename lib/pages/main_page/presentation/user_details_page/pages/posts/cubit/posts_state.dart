part of 'posts_cubit.dart';

@immutable
abstract class PostsState extends Equatable {}

class PostsInitial extends PostsState {
  @override
  List<Object?> get props => [];
}

class PostsLoadingState extends PostsState {
  @override
  List<Object?> get props => [];
}

class PostsLoadedState extends PostsState {
  final List<PostModel> postModels;

  PostsLoadedState(this.postModels);

  @override
  List<Object?> get props => [postModels];
}

class PostsLoadingErrorState extends PostsState {
  final String errorMessage;

  PostsLoadingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}
