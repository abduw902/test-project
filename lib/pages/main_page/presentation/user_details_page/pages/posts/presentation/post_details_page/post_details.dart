import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/model/posts_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/comments_list.dart';

class PostDetails extends StatefulWidget {
  final PostModel postModel;

  const PostDetails({Key? key, required this.postModel}) : super(key: key);

  @override
  _PostDetailsState createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: 'Post info',
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                widget.postModel.title!,
                style: kMediumBlack14,
              ),
            ),
            verticalSpace5,
            Text(
              widget.postModel.body!,
              style: kRegularGrey14,
            ),
            verticalSpace8,
            const CustomDivider(),
            verticalSpace8,
            const Text(
              'Comments:',
              style: kSemiBoldBlack16,
            ),
            verticalSpace8,
            CommentsList(postId: widget.postModel.id!),
          ],
        ),
      ),
    );
  }
}
