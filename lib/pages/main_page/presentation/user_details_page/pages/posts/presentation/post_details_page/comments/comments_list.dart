import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/core/components/loading_widget.dart';
import 'package:test_project/core/components/modal_bottom_sheet.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/core/utils/toast_utils.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/add_comment.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/cubit/comments_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/commnet_container.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/repository/comments_repository.dart';

class CommentsList extends StatefulWidget {
  final int postId;

  const CommentsList({Key? key, required this.postId}) : super(key: key);

  @override
  _CommentsListState createState() => _CommentsListState();
}

class _CommentsListState extends State<CommentsList> {
  late CommentsCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = CommentsCubit(CommentsRepositoryImplements())
      ..getPostsOfUser(postId: widget.postId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CommentsCubit, CommentsState>(
      bloc: _cubit,
      listener: (context, state) {
        if (state is CommentsLoadingErrorState) {
          ToastUtils.showShortToast(context, state.errorMessage);
        }
      },
      builder: (context, state) {
        if (state is CommentsLoadingState) {
          return const BuildLoading();
        }
        return Column(
          children: [
            ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return CommentContainer(
                      commentModel: _cubit.commentModels[index]);
                },
                separatorBuilder: (context, index) => verticalSpace8,
                itemCount: _cubit.commentModels.length),
            verticalSpace8,
            GestureDetector(
              onTap: () async {
                await customModalBottomSheet(
                  context,
                  'Add comment',
                  AddComment(
                    cubit: _cubit,
                    postId: widget.postId,
                    lastCommentId:
                        _cubit.commentModels[_cubit.commentModels.length-1].id!,
                  ),
                );
                setState(() {});
              },
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12), color: kBlue),
                child: Text(
                  'Add comment',
                  style: kMediumBlack16.copyWith(color: kWhite),
                ),
              ),
            ),
          ],
        );
        ;
      },
    );
  }
}
