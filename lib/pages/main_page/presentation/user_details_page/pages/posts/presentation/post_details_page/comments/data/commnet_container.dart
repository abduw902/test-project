import 'package:flutter/material.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/model/comments_model.dart';

class CommentContainer extends StatelessWidget {
  final CommentModel commentModel;

  const CommentContainer({Key? key, required this.commentModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10.0,
      ),
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: kBlue.withOpacity(0.15)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(commentModel.name!, style: kMediumBlack14),
            verticalSpace5,
            Text(commentModel.email!, style: kRegularBlue14),
            verticalSpace5,
            Text(commentModel.body!, style: kRegularGrey14),
          ],
        ),
      ),
    );
  }
}
