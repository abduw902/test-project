import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/model/comments_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/repository/comments_repository.dart';

part 'comments_state.dart';

class CommentsCubit extends Cubit<CommentsState> {
  final CommentsRepository repository;

  CommentsCubit(this.repository) : super(CommentsInitial());

  List<CommentModel> commentModels = [];

  Future<void> getPostsOfUser({required int postId}) async {
    try {
      commentModels = [];
      emit(CommentsLoadingState());
      final result = await repository.getCommentsForPost(postId);
      commentModels = result;
      emit(CommentsLoadedState(result));
    } on ServerException {
      emit(CommentsLoadingErrorState(serverErrorMsg));
    } on SocketException {
      emit(CommentsLoadingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(CommentsLoadingErrorState(e.toString()));
    }
  }

  Future<void> addComment({required CommentModel commentModel}) async {
    try {
      emit(CommentsAddingState());
      await repository.addComment(commentModel);
      commentModels.add(commentModel);
      emit(CommentsAddedState());
    } on ServerException {
      emit(CommentsAddingErrorState(serverErrorMsg));
    } on SocketException {
      emit(CommentsAddingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(CommentsAddingErrorState(e.toString()));
    }
  }
}
