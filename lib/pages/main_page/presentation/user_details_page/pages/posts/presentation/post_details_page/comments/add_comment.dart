import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_text_field.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/core/utils/toast_utils.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/cubit/comments_cubit.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/model/comments_model.dart';

class AddComment extends StatefulWidget {
  final CommentsCubit cubit;
  final int lastCommentId;
  final int postId;

  const AddComment({
    Key? key,
    required this.cubit,
    required this.lastCommentId,
    required this.postId,
  }) : super(key: key);

  @override
  _AddCommentState createState() => _AddCommentState();
}

class _AddCommentState extends State<AddComment> {
  CommentModel commentModel = CommentModel();
  late TextEditingController _nameController,
      _emailController,
      _commentController;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _commentController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: Column(
        children: [
          verticalSpace10,
          CustomTextField(
            controller: _nameController,
            textInputAction: TextInputAction.next,
            hintText: 'name',
          ),
          verticalSpace10,
          CustomTextField(
            controller: _emailController,
            hintText: 'email',
          ),
          verticalSpace10,
          CustomTextField(
            controller: _commentController,
            hintText: 'comment',
            textInputAction: TextInputAction.done,
          ),
          const Spacer(),
          GestureDetector(
            onTap: () {
              if (_commentController.text.isNotEmpty &&
                  _emailController.text.isNotEmpty &&
                  _nameController.text.isNotEmpty) {
                setupCommentsModel();
                widget.cubit.addComment(commentModel: commentModel);
                Navigator.pop(context);
              } else {
                ToastUtils.showShortToast(context, 'Fields should be filled');
              }
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12), color: kBlue),
              child: Text(
                'Add comment',
                style: kMediumBlack16.copyWith(color: kWhite),
              ),
            ),
          ),
          verticalSpace16,
        ],
      ),
    );
  }

  void setupCommentsModel() {
    commentModel.email = _emailController.text;
    commentModel.name = _nameController.text;
    commentModel.body = _commentController.text;
    commentModel.postId = widget.postId;
    commentModel.id = widget.lastCommentId + 1;
  }
}
