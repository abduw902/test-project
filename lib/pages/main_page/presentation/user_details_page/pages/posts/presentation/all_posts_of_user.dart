import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/model/posts_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/post_details.dart';

class AllPostsOfUser extends StatefulWidget {
  final List<PostModel> postModels;

  const AllPostsOfUser({Key? key, required this.postModels}) : super(key: key);

  @override
  _AllPostsOfUserState createState() => _AllPostsOfUserState();
}

class _AllPostsOfUserState extends State<AllPostsOfUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      appBar: CustomAppBar(
        title: "All posts",
      ),
      body: ListView.separated(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          itemBuilder: (context, index) {
            return GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PostDetails(
                            postModel: widget.postModels[index],
                          ))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      widget.postModels[index].title!,
                      style: kMediumBlack14,
                    ),
                  ),
                  verticalSpace5,
                  Text(
                    widget.postModels[index].body!,
                    style: kRegularGrey14,
                  )
                ],
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Column(
              children: const [
                verticalSpace8,
                CustomDivider(),
                verticalSpace8,
              ],
            );
          },
          itemCount: widget.postModels.length),
    );
  }
}
