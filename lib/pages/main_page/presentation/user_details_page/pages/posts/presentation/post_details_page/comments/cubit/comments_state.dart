part of 'comments_cubit.dart';

@immutable
abstract class CommentsState extends Equatable {}

class CommentsInitial extends CommentsState {
  @override
  List<Object?> get props => [];
}

class CommentsLoadingState extends CommentsState {
  @override
  List<Object?> get props => [];
}

class CommentsLoadedState extends CommentsState {
  final List<CommentModel> commentModels;

  CommentsLoadedState(this.commentModels);

  @override
  List<Object?> get props => [commentModels];
}

class CommentsLoadingErrorState extends CommentsState {
  final String errorMessage;

  CommentsLoadingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}

class CommentsAddingState extends CommentsState {
  @override
  List<Object?> get props => [];
}

class CommentsAddedState extends CommentsState {
  @override
  List<Object?> get props => [];
}

class CommentsAddingErrorState extends CommentsState {
  final String errorMessage;

  CommentsAddingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}
