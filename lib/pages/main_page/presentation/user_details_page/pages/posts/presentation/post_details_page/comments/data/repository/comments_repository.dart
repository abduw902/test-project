import 'dart:convert';

import 'package:test_project/core/utils/api_client.dart';
import 'package:test_project/core/utils/constants/api.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/presentation/post_details_page/comments/data/model/comments_model.dart';

abstract class CommentsRepository {
  Future<List<CommentModel>> getCommentsForPost(int postId);

  Future<void> addComment(CommentModel commentModel);
}

class CommentsRepositoryImplements implements CommentsRepository {
  @override
  Future<List<CommentModel>> getCommentsForPost(int postId) async {
    final result =
        await ApiClient.getRequest('${API.URL}/comments?postId=$postId');
    return commentModelsFromJson(jsonEncode(result));
  }

  @override
  Future<void> addComment(CommentModel commentModel) async {
    await ApiClient.postRequest('${API.URL}/comments', commentModel.toJson());
  }
}
