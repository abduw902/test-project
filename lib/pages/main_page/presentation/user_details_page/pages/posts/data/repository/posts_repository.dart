import 'dart:convert';

import 'package:test_project/core/utils/api_client.dart';
import 'package:test_project/core/utils/constants/api.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/posts/data/model/posts_model.dart';

abstract class PostsRepository {
  // Future<List<PostModel>> getAllPosts();

  Future<List<PostModel>> getPostsOfUser(int userId);
}

class PostsRepositoryImplements implements PostsRepository {
  // @override
  // Future<List<PostModel>> getAllPosts() async {
  //   final result = await ApiClient.getRequest('${API.URL}/posts');
  //   return postModelsFromJson(jsonEncode(result));
  // }

  @override
  Future<List<PostModel>> getPostsOfUser(int userId) async {
    final result =
        await ApiClient.getRequest('${API.URL}/posts?userId=$userId');
    return postModelsFromJson(jsonEncode(result));
  }
}
