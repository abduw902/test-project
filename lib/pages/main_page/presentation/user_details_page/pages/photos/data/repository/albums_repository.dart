import 'dart:convert';

import 'package:test_project/core/utils/api_client.dart';
import 'package:test_project/core/utils/constants/api.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/data/model/photos_model.dart';

abstract class PhotosRepository {
  Future<List<PhotoModel>> getAlbumPhotos(int albumId);
}

class PhotosRepositoryImplements implements PhotosRepository {
  @override
  Future<List<PhotoModel>> getAlbumPhotos(int albumId) async {
    final result =
        await ApiClient.getRequest('${API.URL}/photos?albumId=$albumId');
    return photoModelsFromJson(jsonEncode(result));
  }
}
