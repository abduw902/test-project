import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/data/model/photos_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/data/repository/albums_repository.dart';

part 'photos_state.dart';

class PhotosCubit extends Cubit<PhotosState> {
  final PhotosRepository repository;

  PhotosCubit(this.repository) : super(PhotosInitial());

  Future<void> getAlbumPhotos({required int albumId}) async {
    try {
      emit(AlbumPhotosLoadingState());
      final result = await repository.getAlbumPhotos(albumId);
      emit(AlbumPhotosLoadedState(result));
    } on ServerException {
      emit(AlbumPhotosLoadingErrorState(serverErrorMsg));
    } on SocketException {
      emit(AlbumPhotosLoadingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(AlbumPhotosLoadingErrorState(e.toString()));
    }
  }
}
