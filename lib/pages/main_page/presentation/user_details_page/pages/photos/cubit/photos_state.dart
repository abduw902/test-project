part of 'photos_cubit.dart';

@immutable
abstract class PhotosState extends Equatable {}

class PhotosInitial extends PhotosState {
  @override
  List<Object?> get props => [];
}

class AlbumPhotosLoadingState extends PhotosState {
  @override
  List<Object?> get props => [];
}

class AlbumPhotosLoadedState extends PhotosState {
  final List<PhotoModel> photoModels;

  AlbumPhotosLoadedState(this.photoModels);

  @override
  List<Object?> get props => [photoModels];
}

class AlbumPhotosLoadingErrorState extends PhotosState {
  final String errorMessage;

  AlbumPhotosLoadingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}
