part of 'albums_cubit.dart';

@immutable
abstract class AlbumsState extends Equatable {}

class AlbumsInitial extends AlbumsState {
  @override
  List<Object?> get props => [];
}

class AlbumsLoadingState extends AlbumsState {
  @override
  List<Object?> get props => [];
}

class AlbumsLoadedState extends AlbumsState {
  final List<AlbumModel> albumModels;

  AlbumsLoadedState(this.albumModels);

  @override
  List<Object?> get props => [albumModels];
}

class AlbumsLoadingErrorState extends AlbumsState {
  final String errorMessage;

  AlbumsLoadingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}
