import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/model/albums_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/repository/albums_repository.dart';

part 'albums_state.dart';

class AlbumsCubit extends Cubit<AlbumsState> {
  final AlbumsRepository repository;

  AlbumsCubit(this.repository) : super(AlbumsInitial());

  Future<void> getAlbumsOfUser({required int userId}) async {
    try {
      emit(AlbumsLoadingState());
      final result = await repository.getUserAlbums(userId);
      emit(AlbumsLoadedState(result));
    } on ServerException {
      emit(AlbumsLoadingErrorState(serverErrorMsg));
    } on SocketException {
      emit(AlbumsLoadingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(AlbumsLoadingErrorState(e.toString()));
    }
  }

}
