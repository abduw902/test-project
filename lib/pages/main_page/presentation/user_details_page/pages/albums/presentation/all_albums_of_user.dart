import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/custom_divider.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/model/albums_model.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/widgets/photos_list.dart';

class AllAlbumsOfUser extends StatefulWidget {
  final List<AlbumModel> albumModels;

  const AllAlbumsOfUser({Key? key, required this.albumModels})
      : super(key: key);

  @override
  _AllAlbumsOfUserState createState() => _AllAlbumsOfUserState();
}

class _AllAlbumsOfUserState extends State<AllAlbumsOfUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhite,
      appBar: CustomAppBar(
        title: "All albums",
      ),
      body: ListView.separated(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          itemBuilder: (context, index) {
            return Column(
              children: [
                Text(
                  widget.albumModels[index].title!,
                  style: kMediumBlack14,
                ),
                verticalSpace8,
                PhotosList(albumId: widget.albumModels[index].id!)
              ],
            );
          },
          separatorBuilder: (context, index) {
            return Column(
              children: const [
                verticalSpace8,
                CustomDivider(),
                verticalSpace8,
              ],
            );
          },
          itemCount: widget.albumModels.length),
    );
  }
}
