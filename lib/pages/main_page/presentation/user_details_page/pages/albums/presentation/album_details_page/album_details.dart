import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';

import 'package:flutter/material.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/shimmer_widget.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/photos/data/model/photos_model.dart';

class AlbumDetails extends StatelessWidget {
  final List<PhotoModel> photoModels;
  final int? startsWith;
  final Color? backgroundColor;

  const AlbumDetails({
    Key? key,
    required this.photoModels,
    this.startsWith,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: backgroundColor ?? kBlack,
      appBar: CustomAppBar(
        isBackgroundColorTransparent: true,
      ),
      body: Column(
        children: [
          GestureDetector(
            onVerticalDragUpdate: (details) {
              if (!details.delta.dy.isNegative && details.delta.dy > 10) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              height: size.height,
              child: Swiper(
                index: startsWith ?? 0,
                loop: false,
                itemCount: photoModels.length,
                itemBuilder: (context, index) {
                  return CachedNetworkImage(
                    fit: BoxFit.contain,
                    imageUrl: photoModels[index].url!,
                    placeholder: (context, _) => ShimmerWidget.rectangular(
                        height: size.height,
                        shapeBorder: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0))),
                    errorWidget: (ctx, _, __) {
                      return const Text('image loading failed');
                    },
                  );
                },
                pagination: SwiperCustomPagination(builder:
                    (BuildContext context, SwiperPluginConfig? config) {
                  return Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      Positioned(
                        right: 16,
                        bottom: 50,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 6),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: kGrey.withOpacity(0.5),
                          ),
                          child: Row(
                            key: key,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                '${config!.activeIndex! + 1}',
                                style: kRegularBlack14.copyWith(
                                    color: kWhite, fontSize: 12),
                              ),
                              Text(
                                ' / ${config.itemCount}',
                                style: kRegularBlack14.copyWith(
                                    color: kWhite, fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 100,
                          child: SizedBox(
                            width:250,
                            child: Text(
                              photoModels[config.activeIndex!].title!,
                              textAlign: TextAlign.center,
                              style: kRegularBlack14.copyWith(color: kWhite),
                            ),
                          ))
                    ],
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
