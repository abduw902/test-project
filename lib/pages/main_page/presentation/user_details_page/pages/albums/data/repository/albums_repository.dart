import 'dart:convert';

import 'package:test_project/core/utils/api_client.dart';
import 'package:test_project/core/utils/constants/api.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/pages/albums/data/model/albums_model.dart';

abstract class AlbumsRepository {
  Future<List<AlbumModel>> getUserAlbums(int userId);

}

class AlbumsRepositoryImplements implements AlbumsRepository {
  @override
  Future<List<AlbumModel>> getUserAlbums(int userId) async {
    final result =
        await ApiClient.getRequest('${API.URL}/albums?userId=$userId');
    return albumModelsFromJson(jsonEncode(result));
  }
}
