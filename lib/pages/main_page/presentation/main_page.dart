import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/core/components/custom_appbar.dart';
import 'package:test_project/core/components/loading_widget.dart';
import 'package:test_project/core/components/style.dart';
import 'package:test_project/core/components/ui_helpers.dart';
import 'package:test_project/core/utils/toast_utils.dart';
import 'package:test_project/pages/main_page/cubit/users_cubit.dart';
import 'package:test_project/pages/main_page/data/model/users_model.dart';
import 'package:test_project/pages/main_page/data/repository/users_repository.dart';
import 'package:test_project/pages/main_page/presentation/user_details_page/user_details_page.dart';
import 'package:test_project/pages/main_page/presentation/widgets/user_card.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late UsersCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = UsersCubit(UsersRepositoryImplements());
    _cubit.getAllUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kLightGrey,
      appBar: CustomAppBar(
        title: "Main page",
      ),
      body: BlocConsumer<UsersCubit, UsersState>(
        bloc: _cubit,
        listener: (context, state) {
          if (state is UsersLoadingErrorState) {
            ToastUtils.showShortToast(context, state.errorMessage);
          }
        },
        builder: (context, state) {
          if (state is UsersLoadedState) {
            List<UserModel> userModels = state.userModels;
            return RefreshIndicator(
              onRefresh: () => _cubit.getAllUsers(),
              child: ListView.separated(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UserDetailsPage(
                                    userModel: userModels[index]))),
                        child: UserCard(
                            user: userModels[index].name!,
                            userName: userModels[index].username!));
                  },
                  separatorBuilder: (context, index) => verticalSpace8,
                  itemCount: state.userModels.length),
            );
          }
          return const BuildLoading();
        },
      ),
    );
  }
}
