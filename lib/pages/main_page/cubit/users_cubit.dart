import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:test_project/core/error/exception.dart';
import 'package:test_project/core/utils/constants/error_messages.dart';
import 'package:test_project/pages/main_page/data/model/users_model.dart';
import 'package:test_project/pages/main_page/data/repository/users_repository.dart';

part 'users_state.dart';

class UsersCubit extends Cubit<UsersState> {
  final UsersRepository repository;

  UsersCubit(this.repository) : super(UsersInitial());

  Future<void> getAllUsers() async {
    try {
      emit(UsersLoadingState());
      final result = await repository.getAllUsers();
      emit(UsersLoadedState(result));
    } on ServerException {
      emit(UsersLoadingErrorState(serverErrorMsg));
    } on SocketException {
      emit(UsersLoadingErrorState(noInternetErrorMsg));
    } catch (e) {
      emit(UsersLoadingErrorState(e.toString()));
    }
  }
}
