part of 'users_cubit.dart';

@immutable
abstract class UsersState extends Equatable {}

class UsersInitial extends UsersState {
  @override
  List<Object?> get props => [];
}

class UsersLoadingState extends UsersState {
  @override
  List<Object?> get props => [];
}

class UsersLoadedState extends UsersState {
  final List<UserModel> userModels;

  UsersLoadedState(this.userModels);

  @override
  List<Object?> get props => [userModels];
}

class UsersLoadingErrorState extends UsersState {
  final String errorMessage;

  UsersLoadingErrorState(this.errorMessage);

  @override
  List<Object?> get props => [];
}
