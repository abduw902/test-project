import 'dart:convert';

import 'package:test_project/core/utils/api_client.dart';
import 'package:test_project/core/utils/constants/api.dart';
import 'package:test_project/pages/main_page/data/model/users_model.dart';

abstract class UsersRepository {
  Future<List<UserModel>> getAllUsers();
}

class UsersRepositoryImplements implements UsersRepository {
  @override
  Future<List<UserModel>> getAllUsers() async {
    final response = await ApiClient.getRequest('${API.URL}/users');
    return userModelsFromJson(jsonEncode(response));
  }
}
